Simple Play Framework Redis plugin
==================================

Based on Typesafe Redis plugin but simplified to get rid of unchecked casting. That way plugin persists only
String objects. But together with Pickling plugin it can be very effective. And it is type safe ;)

## All thanks to...

Typesafe Redis plugin that is used as Cache plugin.

# Usage

Currently you have to download this package, go into plugin folder and execute ```sbt publish-local``` or ```play publish-local``` command.

Then, you need to add tis plugin to your final project. To do so:

1. Open your_project/built.sbt and add dependency:

    ```"com.mdraco" % "mdraco-redis-plugin_2.10" % "0.2.0"```

2. Optional, but recommended, add also pickling:

    ```"org.scala-lang" %% "scala-pickling" % "0.8.0-SNAPSHOT"```

    You also need to add the Sonatype "snapshots" repository resolver to your build file:

    ```resolvers += Resolver.sonatypeRepo("snapshots") ```

3. If you want to be a real plugin create or open your_project/conf/play.plugins and add this line:

    ```"1000:com.mdraco.redis.RedisPlugin"```

    Without it you may access all functions via Redis object (just like in sample).

All this configuration can be seen on sample application.

# Links

[Redis plugin by Typesafe](https://github.com/typesafehub/play-plugins/tree/master/redis)

[Pickling plugin](https://github.com/scala/pickling)
