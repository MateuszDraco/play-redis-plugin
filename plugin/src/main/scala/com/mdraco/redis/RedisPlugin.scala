package com.mdraco.redis

import play.api.Plugin
import play.api.Application
import play.api.Play.current
import scala.collection.JavaConverters._

trait RedisApi {
  def set(key: String, value: String, expiration: Int)

  def get(key: String): Option[String]

  def remove(key: String)

  def listPush(listId: String, value: String)

  def listPop(listId: String): String

  def listLength(listId: String): Long

  def listRange(listId: String, start: Long, end: Long): List[String]

  def hashKeys(hashId: String): List[String]

  def hashSet(hashId: String, key: String, value: String)

  def hashGet(hashId: String, key: String): Option[String]

  def hashRemove(hashId: String, key: String)

  def hashValues(hashId: String): List[String]
}

object Redis extends RedisApi {
  private def getRedisApi(implicit app: Application): RedisApi = {
    app.plugin[RedisPlugin] match {
      case Some(plugin) => plugin
      case None =>
        //throw new Exception("You should register Redis plugin.")
        println("Plugin is not registered. Creating new object...")
        new RedisPlugin(app)
    }
  }

  lazy val redisApi = getRedisApi

  override def set(key: String, value: String, expiration: Int = 0) = redisApi.set(key, value, expiration)

  override def get(key: String): Option[String] = redisApi.get(key)

  override def remove(key: String) = redisApi.remove(key)

  override def listRange(listId: String, start: Long, end: Long): List[String] = redisApi.listRange(listId, start, end)

  override def listLength(listId: String): Long = redisApi.listLength(listId)

  override def listPop(listId: String): String = redisApi.listPop(listId)

  override def listPush(listId: String, value: String): Unit = redisApi.listPush(listId, value)

  override def hashValues(hashId: String): List[String] = redisApi.hashValues(hashId)

  override def hashRemove(hashId: String, key: String): Unit = redisApi.hashRemove(hashId, key)

  override def hashGet(hashId: String, key: String): Option[String] = redisApi.hashGet(hashId, key)

  override def hashSet(hashId: String, key: String, value: String): Unit = redisApi.hashSet(hashId, key, value)

  override def hashKeys(hashId: String): List[String] = redisApi.hashKeys(hashId)
}

class RedisPlugin(app: Application) extends Plugin with RedisApi {

  private lazy val pool = new Connector(app)

  override def onStart() {
    pool.sedisPool
  }

  override def onStop() {
    pool.jedisPool.destroy()
  }

  override def set(key: String, value: String, expiration: Int) = pool.sedisPool.withJedisClient { client =>
    client.set(key, value)
    if (expiration > 0)
      client.expire(key, expiration)
  }

  override def get(key: String): Option[String] = pool.sedisPool.withJedisClient { client =>
    client.get(key)
  } match {
    case null => None
    case value: String => Some(value)
  }

  override def remove(key: String) = pool.sedisPool.withJedisClient { client =>
    client.del(key)
  }

  override def listPush(listId: String, value: String) = pool.sedisPool.withJedisClient { client =>
    client.lpush(listId, value)
  }

  override def listPop(listId: String): String = pool.sedisPool.withJedisClient { client =>
    //println(s"listPop($listId)")
    client.lpop(listId)
  }

  override def listLength(listId: String): Long = pool.sedisPool.withJedisClient {
    client =>
      client.llen(listId)
  }

  override def listRange(listId: String, start: Long, end: Long): List[String] = pool.sedisPool.withJedisClient {
    client =>
      client.lrange(listId, start, end).asScala.toList
  }

  override def hashValues(hashId: String): List[String] = pool.sedisPool.withJedisClient {
    client =>
      client.hvals(hashId).asScala.toList
  }

  override def hashRemove(hashId: String, key: String): Unit = pool.sedisPool.withJedisClient {
    client =>
      client.hdel(hashId, key)
  }

  override def hashGet(hashId: String, key: String): Option[String] = pool.sedisPool.withJedisClient {
    client =>
      client.hget(hashId, key) match {
        case data: String => Some(data)
        case _ => None
      }
  }

  override def hashSet(hashId: String, key: String, value: String): Unit = pool.sedisPool.withJedisClient {
    client =>
      client.hset(hashId, key, value)
  }

  override def hashKeys(hashId: String): List[String] = pool.sedisPool.withJedisClient {
    client =>
      client.hkeys(hashId).asScala.toList
  }
}