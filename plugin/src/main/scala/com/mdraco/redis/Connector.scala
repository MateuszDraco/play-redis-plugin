package com.mdraco.redis

import play.api.{Logger, Application}
import org.apache.commons.lang3.builder.ReflectionToStringBuilder
import redis.clients.jedis.{JedisPoolConfig, JedisPool}
import java.net.URI
import org.sedis.Pool
import org.apache.commons.pool.impl.GenericObjectPool

/**
 *
 * User: mateusz
 * Date: 16.02.2014
 * Time: 18:52
 * Created with IntelliJ IDEA.
 */
class Connector(app: Application) {

  /**
   * provides access to the underlying jedis Pool
   */
  lazy val jedisPool = {
    val poolConfig = createPoolConfig(app)
    Logger.info(s"Redis Plugin enabled. Connecting to Redis on ${host}:${port} with timeout ${timeout}.")
    Logger.info("Redis Plugin pool configuration: " + new ReflectionToStringBuilder(poolConfig).toString())
    new JedisPool(poolConfig, host, port, timeout, password)
  }

  private lazy val redisUri = app.configuration.getString("redis.uri").map {
    new URI(_)
  }

  private lazy val host = app.configuration.getString("redis.host")
    .orElse(redisUri.map {
    _.getHost()
  })
    .getOrElse("localhost")

  private lazy val port = app.configuration.getInt("redis.port")
    .orElse(redisUri.map {
    _.getPort()
  }.filter {
    _ != -1
  })
    .getOrElse(6379)

  private lazy val password = app.configuration.getString("redis.password")
    .orElse(redisUri.map {
    _.getUserInfo()
  }.filter {
    _ != null
  }.filter {
    _ contains ":"
  }.map {
    _.split(":", 2)(1)
  })
    .getOrElse(null)

  private lazy val timeout = app.configuration.getInt("redis.timeout")
    .getOrElse(2000)

  /**
   * provides access to the sedis Pool
   */
  lazy val sedisPool = new Pool(jedisPool)

  private def createPoolConfig(app: Application): JedisPoolConfig = {
    val poolConfig: JedisPoolConfig = new JedisPoolConfig()
    app.configuration.getInt("redis.pool.maxIdle").map {
      poolConfig.maxIdle = _
    }
    app.configuration.getInt("redis.pool.minIdle").map {
      poolConfig.minIdle = _
    }
    app.configuration.getInt("redis.pool.maxActive").map {
      poolConfig.maxActive = _
    }
    app.configuration.getInt("redis.pool.maxWait").map {
      poolConfig.maxWait = _
    }
    app.configuration.getBoolean("redis.pool.testOnBorrow").map {
      poolConfig.testOnBorrow = _
    }
    app.configuration.getBoolean("redis.pool.testOnReturn").map {
      poolConfig.testOnReturn = _
    }
    app.configuration.getBoolean("redis.pool.testWhileIdle").map {
      poolConfig.testWhileIdle = _
    }
    app.configuration.getLong("redis.pool.timeBetweenEvictionRunsMillis").map {
      poolConfig.timeBetweenEvictionRunsMillis = _
    }
    app.configuration.getInt("redis.pool.numTestsPerEvictionRun").map {
      poolConfig.numTestsPerEvictionRun = _
    }
    app.configuration.getLong("redis.pool.minEvictableIdleTimeMillis").map {
      poolConfig.minEvictableIdleTimeMillis = _
    }
    app.configuration.getLong("redis.pool.softMinEvictableIdleTimeMillis").map {
      poolConfig.softMinEvictableIdleTimeMillis = _
    }
    app.configuration.getBoolean("redis.pool.lifo").map {
      poolConfig.lifo = _
    }
    app.configuration.getString("redis.pool.whenExhaustedAction").map {
      setting =>
        poolConfig.whenExhaustedAction = setting match {
          case "fail" | "0" => GenericObjectPool.WHEN_EXHAUSTED_FAIL
          case "block" | "1" => GenericObjectPool.WHEN_EXHAUSTED_BLOCK
          case "grow" | "2" => GenericObjectPool.WHEN_EXHAUSTED_FAIL
        }
    }
    poolConfig
  }

}
