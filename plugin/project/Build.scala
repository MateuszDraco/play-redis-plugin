import sbt._
import Keys._

object MinimalBuild extends Build {

  lazy val pk11 = "pk11 repo" at "http://pk11-scratch.googlecode.com/svn/trunk"
  lazy val typesafe = "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

  lazy val root = Project(id = "mdraco-redis-plugin", base = file("."), settings = Project.defaultSettings).settings(
    version := "0.3.1",
    scalaVersion := "2.10.3",
    organization := "com.mdraco",
    resolvers += pk11,
    resolvers += typesafe,
    libraryDependencies += "com.typesafe.play" %% "play" % "2.2.1" % "provided",
    libraryDependencies += "com.typesafe.play" %% "play-test" % "2.2.1" % "provided",
    libraryDependencies += "biz.source_code" % "base64coder" % "2010-12-19",
    libraryDependencies += "org.sedis" % "sedis_2.10.0" % "1.1.1"
    //libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.1.0" % "test"
  )
}