# Simple Play Framework Redis plugin
=====================================

Based on Typesafe Redis plugin but simplified to get rid of unchecked casting. That way plugin persists only
String objects. But together with Pickling plugin it can be very effective. And it is type safe ;)
