package models

import akka.actor.Actor
import com.mdraco.redis.Redis
import scala.pickling._
import json._


/**
 *
 * User: mateusz
 * Date: 16.02.2014
 * Time: 19:50
 * Created with IntelliJ IDEA.
 */

trait TaskMessage
case object FetchAll extends TaskMessage
trait Task extends TaskMessage {
  val id: String
  val present: String
}
case class TextTask(id: String, name: String) extends Task {
  val present = s"[str: $name]"
}
case class IntTask(id: String, num: Int) extends Task {
  val present = s"[int: $num]"
}
case object NullTask extends Task {
  val id = "null"
  val present = "[null]"
}
case class TaskToRemove(id: String)

class TaskActor extends Actor {

  private def get(id: String): Task = Redis.get("task:" + id) match {
    case Some(data) => JSONPickle(data).unpickle[Task]
    case None => TextTask("???", "???")
  }

  private def all: List[String] = Redis.get("tasks") match {
    case Some(data) => JSONPickle(data).unpickle[List[String]]
    case None => List()
  }

  override def receive: Actor.Receive = {
    case FetchAll =>
      sender ! all.map {
        id => get(id)
      }
    case t: Task => {
      Redis.set("task:" + t.id, t.pickle.value)
      Redis.set("tasks", (all.filter(_ != t.id) :+ t.id).pickle.value)
    }
    case TaskToRemove(id) => {
      Redis.remove("task:"+id)
      Redis.set("tasks", all.filter(_ != id).pickle.value)
    }
  }
}
