package controllers

import play.api._
import play.api.mvc._
import play.libs.Akka
import akka.actor.Props
import models._
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.data._
import play.api.data.Forms._
import models.TaskToRemove
import java.util.UUID

object Application extends Controller {

  val taskForm = Form(mapping(
    "id" -> nonEmptyText,
    "name" -> nonEmptyText
  )(models.TextTask.apply)(models.TextTask.unapply))

  val numTaskForm = Form(mapping(
    "id" -> nonEmptyText,
    "val" -> number
  )(models.IntTask.apply)(models.IntTask.unapply))

  implicit val timeout = Timeout(5 seconds) // needed for `?` below

  lazy val tasks = Akka.system.actorOf(Props[TaskActor], name = "tasks")

  def index = Action.async {
    (tasks ? FetchAll).mapTo[List[Task]].map { result =>
      val textFilled = taskForm.fill(TextTask(UUID.randomUUID().toString, ""))
      val numFilled = numTaskForm.fill(IntTask(UUID.randomUUID().toString, 0))
      Ok(views.html.index(result, textFilled, numFilled))
    }
  }

  def add = Action { implicit request =>
    taskForm.bindFromRequest.fold(
      withErrors => {},
      data => {
        tasks ! data
      }
    )
    Redirect(routes.Application.index)
  }

  def remove(id: String) = Action {
    tasks ! TaskToRemove(id)
    Redirect(routes.Application.index)
  }

  def addNull = Action {
    tasks ! NullTask
    Redirect(routes.Application.index)
  }


  def addInt = Action {
    implicit request =>
      numTaskForm.bindFromRequest.fold(
        withErrors => {},
        data => {
          tasks ! data
        }
      )
      Redirect(routes.Application.index)
  }
}