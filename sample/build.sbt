name := "mdraco-redis-plugin-sample"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  anorm,
  "com.mdraco" % "mdraco-redis-plugin_2.10" % "0.1.2",
  "org.scala-lang" %% "scala-pickling" % "0.8.0-SNAPSHOT"
)

resolvers += Resolver.sonatypeRepo("snapshots")

play.Project.playScalaSettings
